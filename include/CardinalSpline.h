/* 
 * File:   CardinalSpline.h
 * Author: Andoni Aguirrezabal
 *
 */

#ifndef CARDINALSPLINE_H
#define	CARDINALSPLINE_H

#include <vector>

#include <eigen3/Eigen/Core>

#include "SplineDatatypes.hpp"

class CardinalSpline {
public:
    CardinalSpline();
    ~CardinalSpline();

public:
    std::vector< Point<float> > process(const std::vector< Point<int> >& points,
            float tension = 0, int n = 100);

private:
    std::vector< Point<float> > evalCardAtNPlusOneVals(const Point<int>& P, 
            const Point<int>& P1, const Point<int>& P2, const Point<int>& P3, 
            int n);

    Point<float> evalCardAtParam(const Point<int>& P, const Point<int>& P1, 
            const Point<int>& P2, const Point<int>& P3, float u);
    
private:
    Eigen::Matrix4f cardinalMatrix;
};

#endif	/* CARDINALSPLINE_H */

