/* 
 * File:   SplineDatatypes.h
 * Author: andoni
 *
 * Created on June 7, 2015, 3:02 PM
 */

#ifndef SPLINEDATATYPES_H
#define	SPLINEDATATYPES_H

template<typename T>
class Point {
public:
    Point() {
        _x = 0; _y = 0;
    }

//    Point(const Point<T>& _point) {
//        _x = _point.getX();
//        _y = _point.getY();
//    }
    
    Point(T x, T y) {
        _x = x; _y = y;
    }
    
    ~Point() {
    }
    
public:
    T getX(void) const {
        return _x;
    }
        
    T getY(void) const {
        return _y;
    }
    
    void setX(T x) {
        _x = x;
    }
    
    void setY(T y) {
        _y = y;
    }
    
private:
    T _x;
    T _y;
};

#endif	/* SPLINEDATATYPES_H */

