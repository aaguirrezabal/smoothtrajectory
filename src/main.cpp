/* 
 * File:   main.cpp
 * Author: andoni
 *
 * Created on June 6, 2015, 8:23 PM
 */

#include <cstdlib>
#include <iostream>

#include <vector>

#include "CardinalSpline.h"

using namespace std;

int main(int argc, char** argv) {
    CardinalSpline example;
    std::vector< Point<int> > path;
    std::vector< Point<float> > newPath;
    
    path.push_back(Point<int>(450, 450));
    path.push_back(Point<int>(450, 450));
    path.push_back(Point<int>(510, 440));
    path.push_back(Point<int>(520, 435));
    path.push_back(Point<int>(530, 430));
    path.push_back(Point<int>(600, 395));
    path.push_back(Point<int>(630, 395));
    path.push_back(Point<int>(700, 435));
    path.push_back(Point<int>(700, 450));
    path.push_back(Point<int>(700, 450));
    
    std::cout << "----- ORIGINAL PATH -----" << "\n";
    for(int i = 0; i < path.size(); i++) {
        std::cout << i+1 << ": " << path[i].getX() << ", " << path[i].getY() 
                       << "\n";
    }
    
    newPath = example.process(path, 0, 10);
    
    cin.get();
    std::cout << "----- NEW PATH -----" << "\n";
    for(int i = 0; i < newPath.size(); i++) {
        std::cout << i+1 << ": " << newPath[i].getX() << ", " << newPath[i].getY() 
                       << "\n";
    }
    return 0;
}
