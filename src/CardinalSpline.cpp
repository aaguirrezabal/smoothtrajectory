/* 
 * File:   CardinalSpline.cpp
 * Author: andoni
 * 
 * Created on June 6, 2015, 8:28 PM
 */

#include "CardinalSpline.h"

#include <iostream>

CardinalSpline::CardinalSpline() {
}

CardinalSpline::~CardinalSpline() {
}

// Evaluates all points from N to N + 1
std::vector< Point<float> > CardinalSpline::evalCardAtNPlusOneVals(
        const Point<int>& P, const Point<int>& P1, const Point<int>& P2, 
        const Point<int>& P3, int n) 
{
    float du = (float)1/n, u = 0;
    std::vector< Point<float> > outputPoints;
    
    //outputPoints.push_back(evalCardAtParam(P, P1, P2, P3, u));
    for(int i = 1; i <= n; i++) {
        u = i*du;
        outputPoints.push_back(evalCardAtParam(P, P1, P2, P3, u));
    }

    return outputPoints;
}

// Evaluates a singles point, u, from N to N+1
Point<float> CardinalSpline::evalCardAtParam(
        const Point<int>& P, const Point<int>& P1, const Point<int>& P2, 
        const Point<int>& P3, float u)
{
    Eigen::Matrix<float, 4, 1> Gx;
    Eigen::Matrix<float, 4, 1> Gy;
    Eigen::Matrix<float, 1, 4> U;
    Point<float> outputPoint;

    Gx <<  P.getX(),
          P1.getX(),
          P2.getX(),
          P3.getX();

    Gy <<  P.getY(),
          P1.getY(),
          P2.getY(),
          P3.getY();
    
    U << (u*u*u), (u*u), (u), 1;
    
    outputPoint.setX(U*cardinalMatrix*Gx);
    outputPoint.setY(U*cardinalMatrix*Gy);

    return outputPoint;
}

std::vector< Point<float> > CardinalSpline::process(
        const std::vector< Point<int> >& points, 
        float tension, int n)
{
    std::vector< Point<float> > outputPoints;
    float s = (1-tension)/2;
    cardinalMatrix <<  -s, 2-s,     s-2,  s,
                      2*s, s-3, 3-(2*s), -s,
                       -s,   0,       s,  0,
                        0,   1,       0,  0;
    
    // Add the First Point into the output
    Point<float> testPoint;
    testPoint.setX(points[0].getX());
    testPoint.setY(points[0].getY());
    outputPoints.push_back(testPoint);

    for(int i = 0; i < (points.size() - 3); i++) {
        std::vector< Point<float> > XiYi;
        XiYi = evalCardAtNPlusOneVals(points[i], points[i+1], points[i+2], 
                points[i+3], n);
        outputPoints.insert(outputPoints.end(), XiYi.begin(), XiYi.end());
    }
    return outputPoints;
}
